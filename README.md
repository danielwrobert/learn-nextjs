# Learning Next.js #

My working files from walking through the Learning Next.js tutorial - https://learnnextjs.com

### How do I get set up? ###

Clone the repository:
```
git clone https://bitbucket.com/danielwrobert/learn-nextjs.git
cd learn-nextjs
```

Initialize dependencies and run server:
```
npm install
npm run dev
```

### Who do I talk to? ###

* Dan Robert - danielwrobert@gmail.com