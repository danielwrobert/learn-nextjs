import fetch from 'isomorphic-unfetch';
import Markdown from 'react-markdown';
import Layout from '../components/MyLayout.js'

/**
 * Note: The `url` prop is only exposed to the page's main component.
 * It's not exposed for other components used in the page.
 * If you want to use it in another component other than the main
 * component, you will need to pass that down as a prop from the
 * main component.
 */

const Post = ( props ) => (
	<Layout>
		<h1>{ props.show.name }</h1>
		<p>{ props.show.summary.replace( /<[/]?p>/g, '' ) }</p>
		<img src={ props.show.image.medium } alt={ props.show.name } />
		<div className="markdown">
		<Markdown source={`
These are our notes on the show.
Yes. We can have a [link](/link).
And we can have a title as well.

### This is a title

And here's the content.
		`}/>
		</div>
		<style jsx global>{`
			.markdown {
				font-family: 'Arial';
			}

			.markdown a {
				text-decoration: none;
				color: blue;
			}

			.markdown a:hover {
				opacity: 0.6;
			}

			.markdown h3 {
				margin: 0;
				padding: 0;
				text-transform: uppercase;
			}
		`}</style>
	</Layout>
);

Post.getInitialProps = async function( context ) {
	const { id } = context.query;
	const res = await fetch( `https://api.tvmaze.com/shows/${ id }` );
	const show = await res.json();

	console.log( `Fetched show: ${ show.name }` );

	return { show }
}
 
export default Post;